import poster from '../image/poster.jpg'

function MoviePage() {
  return (
    <div>
      <div className="container" style={{ marginTop: "50px" }}>
        <div className="d-flex justify-content-center">
          <div className="top-box">
            <div className="d-flex align-items-center">
              <a href="/">
                <div className="title-name">
                  Movie Review Classification
                </div>
              </a>

            </div>
            <div className="d-flex align-items-center">
              <div className="title-name">
                Year
              </div>
            </div>
            <div className="d-flex align-items-center">
              <div className="title-name">
                Genre
              </div>
            </div>
            <div className="d-flex align-items-center">
              <div className="title-name">
                Rate
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center" style={{ marginTop: "50px" }}>
          <div className="moviename-box">
            {/* <div className="d-flex align-items-center">Movie Name</div> */}
            <div className="moviename-box-title d-flex justify-content-start">
              <div className="title-name">Dunkrik</div>

            </div>
            <div className="title-name-sm d-flex justify-content-start" style={{ padding: "0px 0px 0px 20px" }}>
              Action, Drama, History
            </div>
          </div>
        </div>

        <div className="d-flex justify-content-center" style={{ marginTop: "20px" }}>
          <div className="poster-box" style={{ marginRight: "20px" }}>

            <img src={poster} alt="poster" style={{ width: "100%", height: "auto" }} /> {/* poster image */}

          </div>

          <div className="d-flex flex-column">
            <div className="score-box" style={{ marginBottom: "20px" }}>
              <div className="score-box-title">
                <div className="title-name">
                  From 108 Reviews
                </div>
              </div>

              <div className="d-flex justify-content-center">
                <div style={{ width: "50%" }}>
                  <div className="progress">
                    <div className="progress-bar bg-success progress-bar" role="progressbar" style={{ width: "30%" }} aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                    <div className="progress-bar bg-danger progress-bar" role="progressbar" style={{ width: "70%" }} aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>

              <div className="m-3">
                <div className="title-name-sm">
                  Positive reviews calculate as 29.62 % of all reviews
                                </div>
                <div className="title-name-sm">
                  Negative reviews calculate as 70.38 % of all reviews
                                </div>

              </div>


            </div>
            <div className="desc-box">
              <div className="desc-box-title d-flex justify-content-start">
                <div className="title-name">Description</div>

              </div>
              <div className="px-3 pb-3 pt-2">
                <div className="desc-box-review-detail px-3 pb-3 pt-2">
                  &emsp; Allied soldiers from Belgium, the British Empire, and France are surrounded by the German Army and evacuated during a fierce battle in World War II.
                </div>
              </div>

            </div>
          </div>
        </div>

        <div className="d-flex justify-content-center" style={{ marginTop: "20px" }}>
          <div className="review-box">
            <div className="review-box-title d-flex justify-content-start">
              <div className="title-name">Movie Review</div>
            </div>

            <div>
              <div className="row " style={{ margin: "0px" }}>
                <div className="review-box-sm col-6 p-3">
                  <div style={{ background: "white", width: "100%", height: "100%" }}>
                    <div className="title-name-pos">
                      <div className="title-name-sm"> Positive</div>
                    </div>
                    <div className="title-desc">
                      Last night COL Ferry and I (COL Coldwell, both USA) were able to watch the new WWI film, 1917, before it has national release. It is a cinematographic feast for the eyes, long expansive shots that follow the protagonists as they execute their mission. It does not hide the horrors that existed in trench warfare, it shows them for their brutality and abundance. (My great uncle died as a consequence of his service fighting in the trenches, mustard gas poisoning). In many ways it reminded me of Saving Private Ryan. For those who have served in combat (I  have deployments to Iraq and Afghanistan), I cannot tell you if the film will be too difficult to watch, it might well be, especially if incoming artillery is a trigger. For me, as the camera travels a few inches above the dirt advancing slowly up a berm, my response was visceral. I was taken back to the patrols we walked in Afghanistan, not knowing what was around the corner; not relaxing heightened vigilance, not knowing if there would be an IED, a child wearing a suicide vest, a sniper taking aim. For the protagonists in this film (as for all who served and are serving) surviving the climb up the berm, there is no sigh of relief, no respite from the fear of uncertainty. They (we) survive to move forward to face more uncertainty. Watching allowed me to pay homage to my great uncle, and the approximate 800,000 other Brits who were killed or died as a consequence of their service. (Germany lost over 2 million soldiers in the war). Estimates put the total casualty numbers for both military and civilians at 40 million, half killed or died from wounds/infection. I rate this film as 10/10, for many reasons. Directing, acting, set design, cinematography, musical score, the raw emotion it invokes. Some critics have said they never felt a connection with the characters, I suspect they never served in combat. While the brotherhood (including female War Fighters) is strong, there is also a common characteristic possessed by all War Fighters, the ability to focus on a mission and suppress emotion, even as those around the Fighter fall. This was the quality I recognized in the actors and why the viewer doesn't "bond" with the main protagonists; we, the viewer, were on the mission with them, we grieve as we can and move on. Watch if you will, but know there is no pleasure in watching and the film will grab you and the beginning and not let you go. Even though we know the outcome of WWI, there is no joy, there is no peace. Watch because it will allow you a glimpse at the horror and brutality of war; reflect on their service and sacrifice. Note, as we (the viewer) are "walking" through the trenches, glancing shots of the young soldiers shows them with flat affect, isolation, almost apathy; this is the face of "shell shock," what we know call post-traumatic stress disorder. For original WW1 footage, watch "They Shall Never Grow Old," an exceptional documentary.
                    </div>
                    <a href="/review">
                      <div className="read-more">
                        Read More
                    </div>
                    </a>


                  </div>
                </div>
                <div className="review-box-sm col-6 p-3">
                  <div style={{ background: "white", width: "100%", height: "100%" }}>
                    <div className="title-name-pos">
                      <div className="title-name-sm"> Negative</div>
                    </div>

                  </div>
                </div>
                <div className="review-box-sm col-6 p-3">
                  <div style={{ background: "white", width: "100%", height: "100%" }}>
                    <div className="title-name-pos">
                      <div className="title-name-sm"> Positive</div>
                    </div>

                  </div>
                </div>
                <div className="review-box-sm col-6 p-3">
                  <div style={{ background: "white", width: "100%", height: "100%" }}>
                    <div className="title-name-pos">
                      <div className="title-name-sm"> Negative</div>
                    </div>

                  </div>
                </div>

              </div>


            </div>

          </div>
        </div>

        <div className="d-flex justify-content-center" style={{ marginTop: "50px" }}>
          <div className="contact-box">
            <div className="contact-box-title d-flex justify-content-start">
              <div className="title-name">Contact Us</div>
            </div>
            <div className="p-3">
              <div className="title-name-sm">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean in convallis leo. Integer luctus, erat nec consectetur l
                aoreet, orci nunc lobortis leo, vitae imperdiet orci est at justo. Nam rutrum interdum eros ac ultricies. Quisque sit amet velit
                sit amet quam rhoncus facilisis.
              </div>
            </div>
          </div>
        </div>
        <div style={{ marginBottom: "20px" }}></div>

      </div>
    </div>
  );
}

export default MoviePage;
