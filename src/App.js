import './App.scss';
import MoviePage from './page/moviePage.js';
import MovieReviewDetail from './page/movieReviewDetail.js';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
//import poster from './image/poster.jpg';

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/" exact component={MoviePage} />
          <Route path="/review" exact component={MovieReviewDetail} />
        </Switch>
      </div>
    </Router>

  );
}

export default App;
